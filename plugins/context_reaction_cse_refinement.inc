<?php
// $Id: context_reaction_active_theme.inc,v 1.1.2.1 2010/02/17 20:55:59 brynbellomy Exp $

/**
 * Expose themes as context reactions.
 */
class context_reaction_cse_refinement extends context_reaction {

  /**
   * Allow admins to select from a list of available refinements.
   */
  function options_form($context) {
    
    if ($refinements = explode("\n", variable_get('context_reaction_cse_refinement_settings', ''))) {
      foreach($refinements as $refinement) {
        if ($refinement_pair = explode('|', $refinement)) {
          $options[$refinement_pair[0]] = $refinement_pair[1];
        }
      }
    }

    $form = array(
      '#tree' => TRUE,
      '#title' => t('CSE Refinements'),
      'refinement' => array(
        '#title' => t('CSE Refinement'),
        '#description' => t('Choose a refinement to use when this context is active.'),
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => 0,
      ),
    );
    return $form;
  }

  /**
   * Set the search_refinement variable if needed
   */
  function execute(&$vars) {
    $contexts = context_active_contexts();
		
    foreach ($contexts as $k => $v) {
      if (!empty($v->reactions[$this->plugin]['refinement'])) {
          global $search_refinement;
          $search_refinement = $v->reactions[$this->plugin]['refinement'];
      }
    }
  }

}